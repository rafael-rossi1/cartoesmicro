package br.com.itau.cartoesmicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CartoesmicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartoesmicroApplication.class, args);
	}

}
