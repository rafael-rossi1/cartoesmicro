package br.com.itau.cartoesmicro.repositories;

import br.com.itau.cartoesmicro.models.CreditCard;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCard,Long> {
}
