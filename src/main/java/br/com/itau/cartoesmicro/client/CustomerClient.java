package br.com.itau.cartoesmicro.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLI")
public interface CustomerClient {

    @GetMapping("/{cliente}")
    long getId(@PathVariable long id);

}
