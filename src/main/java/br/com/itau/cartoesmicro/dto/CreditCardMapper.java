package br.com.itau.cartoesmicro.dto;

import br.com.itau.cartoesmicro.client.Customer;
import br.com.itau.cartoesmicro.models.CreditCard;
import org.springframework.stereotype.Component;

@Component
public class CreditCardMapper {


    public CreditCard toCreditCard(CreateCreditCardRequest createCreditCardRequest) {
        Customer customer = new Customer();
        customer.setId(createCreditCardRequest.getCustomerId());
        CreditCard creditCard = new CreditCard();
        creditCard.setNumber(createCreditCardRequest.getNumber());

        return creditCard;
    }

    public CreditCard toCreditCard(UpdateCreditCardRequest updateCreditCardRequest) {
        CreditCard creditCard = new CreditCard();
        creditCard.setActive(updateCreditCardRequest.getActive());
        return creditCard;
    }

    public CreateCreditCardResponse toCreateCreditCardResponse(CreditCard creditCard) {
        CreateCreditCardResponse createCreditCardResponse = new CreateCreditCardResponse();
        createCreditCardResponse.setId(creditCard.getId());
        createCreditCardResponse.setNumber(creditCard.getNumber());
        createCreditCardResponse.setActive(creditCard.getActive());
        createCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        return createCreditCardResponse;
    }

    public GetCreditCardResponse toGetCreditCardResponse(CreditCard creditCard) {
        GetCreditCardResponse getCreditCardResponse = new GetCreditCardResponse();
        getCreditCardResponse.setId(creditCard.getId());
        getCreditCardResponse.setNumber(creditCard.getNumber());
        getCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        return getCreditCardResponse;
    }

    public UpdateCreditCardResponse toUpdateCreditCardResponse(CreditCard creditCard) {
        UpdateCreditCardResponse updateCreditCardResponse = new UpdateCreditCardResponse();
        updateCreditCardResponse.setId(creditCard.getId());
        updateCreditCardResponse.setNumber(creditCard.getNumber());
        updateCreditCardResponse.setActive(creditCard.getActive());
        updateCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        return updateCreditCardResponse;
    }

}
