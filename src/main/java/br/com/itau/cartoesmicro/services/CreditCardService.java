package br.com.itau.cartoesmicro.services;

import br.com.itau.cartoesmicro.client.Customer;
import br.com.itau.cartoesmicro.client.CustomerClient;
import br.com.itau.cartoesmicro.exception.CreditCardNotFoundException;
import br.com.itau.cartoesmicro.models.CreditCard;
import br.com.itau.cartoesmicro.repositories.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CustomerClient customerClient;

    @Autowired
    private CreditCardRepository creditCardRepository;

    public CreditCard create(CreditCard creditCard) {
        creditCard.setCustomerId(customerClient.getId(creditCard.getCustomerId()));
        creditCard.setActive(false);
        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard updatedCreditCard) {
        CreditCard creditCard = getById(updatedCreditCard.getId());

        creditCard.setActive(updatedCreditCard.getActive());

        return creditCardRepository.save(creditCard);
    }

    public CreditCard getById(Long id) {
        Optional<CreditCard> byId = creditCardRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }


}
